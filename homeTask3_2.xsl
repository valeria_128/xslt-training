<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:template match="OTA_HotelAvailRS">
        <OTA_HotelAvailRS>
            <Success />
            <xsl:for-each select="./RoomStays">
                <Hotels>
                    <xsl:for-each select="./RoomStay">
                        <Hotel>
                            <xsl:copy-of select="./@*"/>
                            
                            <xsl:for-each select="./*">
                                <xsl:choose>
                                    <xsl:when test="name(.)='RoomRates'">
                                        <Offers>
                                            <xsl:for-each select="./RoomRate">
                                                <Offer>                             
                                                    <xsl:copy-of select="./@*"/>
                                                    
                                                    <xsl:for-each select="./*">
                                                        <!-- Rates or Total -->
                                                        <xsl:element name="{name(.)}">
                                                            <xsl:for-each select="./@*"> 
                                                                <xsl:call-template name="check_and_round"/>
                                                            </xsl:for-each>
                                                             
                                                             <!-- Rate -->
                                                            <xsl:for-each select="./*">
                                                                <xsl:element name="{name(.)}">
                                                                    <xsl:copy-of select="./@*"/>
                                                                    
                                                                    <!-- Total -->
                                                                    <xsl:for-each select="./*">
                                                                        <xsl:element name="{name(.)}">
                                                                           <xsl:for-each select="./@*"> 
                                                                               <xsl:call-template name="check_and_round"/>
                                                                           </xsl:for-each>
                                                                        </xsl:element>
                                                                    </xsl:for-each>
                                                                </xsl:element>
                                                            </xsl:for-each>  
                                                        </xsl:element>
                                                    </xsl:for-each>
                                                </Offer>
                                            </xsl:for-each>
                                        </Offers>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:copy-of select="."/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:for-each>
                        </Hotel>
                    </xsl:for-each>
                </Hotels>
            </xsl:for-each>
        </OTA_HotelAvailRS>
    </xsl:template>
    
    <xsl:template name="check_and_round">        
        <xsl:attribute name="{name(.)}">
            <xsl:choose>
                <xsl:when test="name(.)='AmountAfterTax'">
                    <xsl:value-of select="round(.)"/>                                                                                
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:template>

</xsl:stylesheet>