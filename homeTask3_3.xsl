<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output method="xml" indent="yes" />
    
    <xsl:variable name="ABC" select="'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'"/>
    
    <xsl:template match="/">
        <list>
            <!-- Строка всех первых символов входного xml. -->
            <xsl:variable name="all_letters">
                <xsl:call-template name="all_first_letters">
                    <xsl:with-param name="index" select="1"/>
                </xsl:call-template>
            </xsl:variable>        
        
            <!-- Формируем алфавитный указатель. -->
            <xsl:call-template name="get_next_letter">
                <xsl:with-param name="index" select="1"/>
                <xsl:with-param name="all_letters" select="$all_letters"/>
            </xsl:call-template>
        </list>        
    </xsl:template>
    
    <!-- Рекурсивно вызываем этот шаблон для каждой буквы из строки ABC. -->
    <xsl:template name="get_next_letter">
        <xsl:param name="index"/>
        <xsl:param name="all_letters"/>
        
        <!-- Следующая буква. -->
        <xsl:variable name="letter" select="substring($ABC, $index, 1)"/>        
        
        <!-- Если эта буква встречается в строке all_letters первых букв элементов файла, -->
        <xsl:if test="contains($all_letters, $letter)">
            
            <!-- тогда выводим все имена из файла, -->
            <capital  value="{$letter}">
                <xsl:for-each select="list/item|@Name">
                    <xsl:sort select="@Name"/>
                    
                    <!-- первая буква которых совпадает с текущей letter из ABC.-->
                    <xsl:if test="substring(@Name, 1, 1) = $letter">
                        <name>
                            <xsl:value-of select="@Name"/>
                        </name>
                    </xsl:if>
                </xsl:for-each>
            </capital>
        </xsl:if>
        
        <xsl:if test="$index != string-length($ABC)"> 
            <xsl:call-template name="get_next_letter">
                <xsl:with-param name="index" select="$index + 1"/>
                <xsl:with-param name="all_letters" select="$all_letters"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <!-- Собираем все первые символы входного xml файла. -->
    <xsl:template name="all_first_letters">
        <xsl:param name="letters"/>
        <xsl:param name="index"/>
        <xsl:variable name="next" select="substring(list/item[$index]/@Name, 1, 1)"/>
        <xsl:value-of select="substring(list/item[$index]/@Name, 1, 1)"/>
        
        <xsl:if test="$index != count(list/item/@Name)">            
            <xsl:call-template name="all_first_letters">
                <xsl:with-param name="letters" select="$letters"/>
                <xsl:with-param name="index" select="$index + 1"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>