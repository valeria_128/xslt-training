<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    
    <xsl:template match="/">
        <!-- Преобразуем все элементы и атрибуты в строку. -->
        <xsl:variable name="all_elements">        
            <xsl:call-template name="all_list">
            </xsl:call-template> 
        </xsl:variable>
        
        <!-- Для каждой новой лексической единицы выведем строку с информацией (первый символ пробел). -->
        <xsl:call-template name="check_all_elements">
            <xsl:with-param name="elements" select="substring-after($all_elements, '&#169;')"/>
        </xsl:call-template>       
    </xsl:template>
    
    <!-- Рекурсивно проверяем строку. При повторном вызове шаблона в параметре element не содержится лекс. ед., которые были обработаны. -->
    <xsl:template name="check_all_elements">
        <xsl:param name="elements"/>
        
        
        <!-- Пока не проверим всю строку. -->
        <xsl:if test="string-length($elements) &gt; 1">
            <xsl:variable name="result">
                
                <!-- Получаем информацию. -->
                <xsl:call-template name="str_pars">
                    <xsl:with-param name="str" select="$elements"/>
                </xsl:call-template>
            </xsl:variable>
            
            <!-- Выводим часть с "Node '...' found .. times."  -->
            <xsl:value-of select="concat(substring-before($result, 'times.'), 'times.')"/>
            
            <!-- Опять обрабатываем строку. -->
            <xsl:call-template name="check_all_elements">
                <xsl:with-param name="elements" select="substring-after($result, 'times.')"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <!-- Склеиваем все названия элементов и атрибутов в одину строку. -->
    <xsl:template name="all_list">
        <xsl:variable name="current_name" select="name(.)"/>        
        <xsl:value-of select="concat($current_name, '&#169;')"/>
        
        <xsl:for-each select="./*|@*">
            <xsl:call-template name="all_list">
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>
    
    <!-- Получаем строку с информацией о лекс. ед. + строку без этой лекс.ед. -->
    <xsl:template name="str_pars">
        <xsl:param name="str"/>
        
        <xsl:if test="string-length($str) &gt; 0">
            <xsl:variable name="result">
                <xsl:call-template name="element_count">
                    <!-- Выделяем лекс. ед. до пробела. -->
                    <xsl:with-param name="name" select="substring-before($str, '&#169;')"/>
                    <!-- Всё, кроме первой лекс. ед. имени. -->
                    <xsl:with-param name="str" select="substring-after($str, '&#169;')"/>
                    <xsl:with-param name="count" select="1"/>
                    <xsl:with-param name="rest" select="$str"/>
                </xsl:call-template>
            </xsl:variable>
            
            <!-- Формируем результат. -->
            <xsl:text>&#10;Node '</xsl:text>
            
            <!-- Выделяем лекс. ед. до пробела. -->
            <xsl:value-of select="substring-before($str, '&#169;')"/>
            <xsl:text>' found </xsl:text>
            
            <!-- Первые символы до пробела в результате - количество лекс. ед. в строке. -->
            <xsl:value-of select="substring-before($result, '&#169;')"/>
            <xsl:text> times.</xsl:text>
            
            <!-- Оставляем количество и вырезаем только строку без лекс. ед., которые только что посчитали. -->
            <xsl:value-of select="substring-after($result, '&#169;')"/>
        </xsl:if>
    </xsl:template>
    
    <!-- Количество лекс. ед. в строке. -->
    <xsl:template name="element_count">
        <xsl:param name="str"/>
        <xsl:param name="name"/>
        <xsl:param name="count"/>
        <xsl:param name="rest"/>
        
        <!-- Вырезаем лекс. ед. и склеиваем её соседей в next. -->
        <xsl:variable name="before" select="substring-before($rest,$name)"/>
        <xsl:variable name="after" select="substring-after(substring-after($rest,$name),'&#169;')"/>
        <xsl:variable name="next" select="concat($before, $after)"/>
        
        <!-- Увеличиваем count и рекурсивно вызываем этот шаблон для next (строка, из которой будут вырезаны все лекс. ед., совпадающие с name.) -->
        <xsl:choose>
            <xsl:when test="string-length(substring-after($str,$name)) &gt; 0">
                <xsl:call-template name="element_count">
                    <xsl:with-param name="str" select="substring-after($str,$name)"/>
                    <xsl:with-param name="name" select="$name"/>
                    <xsl:with-param name="count" select="$count + 1"/>
                    <xsl:with-param name="rest" select="$next"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($count, '&#169;',$next)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>